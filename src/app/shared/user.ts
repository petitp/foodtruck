export class User {
    private _lastName: string;
    public get lastName(): string 
    {
        return this._lastName;
    }
    public set lastName(value: string) 
    {
        this._lastName = value;
    }

    private _firstName: string;
    public get firstName(): string 
    {
        return this._firstName;
    }
    public set firstName(value: string) 
    {
        this._firstName = value;
    }

    private _date: any;
    public get date(): any 
    {
        return this._date;
    }
    public set date(value: any) 
    {
        this._date = value;
    }

    private _formation: string;
    public get formation(): string 
    {
        return this._formation;
    }
    public set formation(value: string) 
    {
        this._formation = value;
    }

    private _action: boolean;
    public get action(): boolean 
    {
        return this._action;
    }
    public set action(value: boolean) 
    {
        this._action = value;
    }

    constructor(lastName?: string, firstName?: string, date?: any, formation?: string, action?: boolean)
    {
        this._lastName = lastName;
        this._firstName = firstName;
        this._date = date;
        this._formation = formation;
        this._action = action;
    }
}