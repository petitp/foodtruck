import { Component, OnInit } from '@angular/core';
import { Products } from './catalogue';
import { CatalogueService } from './catalogue.service';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.css']
})
export class CatalogueComponent implements OnInit {
  searchText:string;
  ngOnInit(): void {
    //throw new Error("Method not implemented.");
  }
  
  listeDesProducts : Array<Products> = new Array<Products>();

  constructor(private serviceCatalogue : CatalogueService) {
    this.serviceCatalogue.getCatalogue().subscribe(reponse=>{
      this.listeDesProducts = reponse as Products[];
   });
  }
}