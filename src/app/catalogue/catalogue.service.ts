import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CatalogueService {
  private productUrl = './assets/api/products/products.json';
  private postUrl = "https://jsonplaceholder.typicode.com/posts";

  constructor(private http: HttpClient) { }

  getCatalogue()
  {
    return this.http.get(this.productUrl);
  }

  getPosts()
  {
    return this.http.get(this.postUrl);
  }
}