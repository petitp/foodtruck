import { Component, OnInit } from '@angular/core';
import { User } from '../shared/user';

@Component({
  selector: 'app-formulaire',
  templateUrl: './formulaire.component.html',
  styleUrls: ['./formulaire.component.scss']
})
export class FormulaireComponent implements OnInit {

  tempUser : User = new User();
  listUser : Array<User> = new Array<User>();
  constructor() { }

  ngOnInit() {
  }

  addUser()
  {
    if(this.tempUser.lastName.length>0 && this.tempUser.firstName.length>0)
    {
      this.listUser.push(this.tempUser);
      this.tempUser = new User();
      console.log(this.listUser);
    }
  }

  removeUser()
  {

  }

  updateUser()
  {
    
  }
}